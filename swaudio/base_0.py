from typing import Any
from pyo import *
import csv 
from datetime import datetime

battery_data = []
with  open('data/battery.csv', newline='') as f:
    reader = csv.reader(f)
    for row in reader:
        try:
            dt, value = row
            battery_data.append((datetime.fromisoformat(dt).timestamp(), float(value)))
        except Exception:
            print('Failed on:', str(row))
            pass

print(f'Loaded {len(battery_data)} battery_data records')


ph_data = []
with  open('data/ph.csv', newline='') as f:
    reader = csv.reader(f)
    for row in reader:
        try:
            dt, value = row
            ph_data.append((datetime.fromisoformat(dt).timestamp(), float(value)))
        except Exception:
            print('Failed on:', str(row))
            pass

print(f'Loaded {len(ph_data)} ph records')

temp_data = []
with  open('data/temperature.csv', newline='') as f:
    reader = csv.reader(f)
    for row in reader:
        try:
            dt, value = row
            temp_data.append((datetime.fromisoformat(dt).timestamp(), float(value)))
        except Exception:
            print('Failed on:', str(row))
            pass

print(f'Loaded {len(temp_data)} tempareture records')


oxygen_data = []
with  open('data/oxygen.csv', newline='') as f:
    reader = csv.reader(f)
    for row in reader:
        try:
            dt, value = row
            oxygen_data.append((datetime.fromisoformat(dt).timestamp(), float(value)))
        except Exception:
            print('Failed on:', str(row))
            pass

print(f'Loaded {len(oxygen_data)} oxyfen records')


class DataStream: 
    def __init__(self, data):
        if len(data) == 0 :
            raise Exception("DataStream fed with an empty data list")
        self._index = -1
        self._data = data
        

    def next(self):
        self._index += 1
        if self._index >= len(self._data):
            self._index = 0
        return self._data[self._index]


    # def __call__(self) -> Any:
    #     datetime, value = self.next()
    #     self._function(datetime, value)

    # def as_pattern(self, time=1):
    #     return Pattern(self, time)


server = Server().boot()
server.amp = 1
server.start()

GLOBAL_FREQ = 10

# signal 1
# lfo = Sine(freq=4, mul=.02, add=1)
fr_base=[100, 99.7]
fr = SigTo(value=fr_base, time=GLOBAL_FREQ*.1, init=0)
lfo = Sine(freq=4)
lf2 = Sine(freq=.25, mul=10, add=30)
signal1 = Blit(freq=fr*lfo, harms=lf2, mul=.05)
signal1.out()

# signal 2
fm_fader = Fader(fadein=0.01, fadeout=GLOBAL_FREQ*.2, dur=GLOBAL_FREQ*.3, mul=.1)
signal2 = FM(carrier=[1000, 1000], ratio=.1, index=5, mul=fm_fader)
signal2.out()


# signal 3
signal3 = Rossler(pitch=[.1, .1], chaos=0.1, mul=.1)
signal3.out()

# signal 4
fm_fader2 = Fader(fadein=GLOBAL_FREQ*.7, fadeout=GLOBAL_FREQ*.1, dur=GLOBAL_FREQ, mul=.4)
lfd4 = Sine([.4,.3], mul=.2, add=.5)
signal4 = SuperSaw(freq=[49,50], detune=lfd4, bal=0.7, mul=fm_fader2)
signal4.out()

ph_stream = DataStream(ph_data)
ox_stream = DataStream(oxygen_data)
battery_stream = DataStream(battery_data)
temp_stream = DataStream(temp_data)

def update():
    _, ph = ph_stream.next()
    _, ox = ox_stream.next()
    _, battery = battery_stream.next()
    _, temp = temp_stream.next()

    print('update', ph, ox, battery, temp)

    fr.setValue(fr_base)
    sph = ((((ph * 100) - 700) / 1000) * .02) +2
    # lfo.setFreq(sph * lf2 * fr)
    # lf2.setFreq( ph / 4)
    signal1.setFreq([sph, sph] * lf2 * fr)

    fm_fader.play()
    signal2.setRatio( ox / 100)
    signal2.setCarrier([battery, battery])

    stemp = temp/30
    signal3.setChaos(stemp)
    signal3.setPitch([1 - stemp, stemp/2])
    
    fm_fader2.play()
    signal4.setFreq([battery/6, battery/5.6])




metro = Metro(GLOBAL_FREQ).play()
tf = TrigFunc(metro, update)

# fm = FM(carrier=1000, ratio=.1, index=5, mul=.2).out()


# def oxygen(datetime, value):
#     scaled = (value - 10) / 10
#     print(f'oxygen {scaled}')
#     # sine_600.setMul(  (value - 10) / 10)
#     fm.ratio += .001

# ox_stream = DataStream(oxygen_data, oxygen)
# p = ox_stream.as_pattern(2)
# p.play()


def gui():
    while(True):
        try:
            x = input()
        except KeyboardInterrupt:
            return
        
if __name__ == '__main__':
    gui()
        