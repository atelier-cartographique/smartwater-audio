# Radio Robot 
  
## commandes pour lancer la radio:
SW_OFFLINE=1 SW_FREQ=1 SW_VOICE=data/VOICE_PIERRE.wav python swaudio/base.py

avec les options
- SW_OFFLINE (1 si offline)
- SW_FREQ (fréquence de rafraichissement, en secondes)
- SW_VOICE (fichier de voix à choisir)

## dépendances:
- pyo
- httpx
