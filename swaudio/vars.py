from collections import namedtuple
import json

Actions = namedtuple("Actions", ["get", "set"])

class Var:
    def __init__(self) -> None:
        self._values = {}

    def register(self, ident, get, set):
        self._values[ident] = Actions(get, set)

    def set(self, ident, value):
        actions = self._values[ident]
        actions.set(value)

    def get(self, ident):
        actions = self._values[ident]
        return actions.get()

    def has(self, ident):
        return ident in self._values

    def save(self, name):
        data = dict()
        for key in self._values.keys():
            data[key] = self.get(key)
        dump = json.dumps(data, indent=2) 
        with open(name, 'w') as file:
            file.write(dump)
            print('Saved')

    def restore(self, name):
        with open(name) as file:
            data = json.load(file)
            for key in data.keys():
                print(f'restore {key} -> {data[key]}')
                if key in self._values:
                    self.set(key, data[key])
                else:
                    print(f'Skipped {key}')
            print('Restored')
        


class Fn:
    def __init__(self) -> None:
        self._fns = {}

    def register(self, ident, fn):
        self._fns[ident] = fn

    def call(self, ident):
        fn = self._fns[ident]
        fn()

    def has(self, ident):
        return ident in self._fns


VAR = Var()
FN = Fn()

