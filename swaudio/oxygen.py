from pyo import *
from vars import VAR, FN

SCALE = "ox/scale"
TRANSLATE = "ox/translate"
FADER = "ox/fader"
METRO = "ox/metro"
OXIMAX = "ox/metro-scale"

scale =  1
translate = -12
oximax = 15

def set_scale(n):
    global scale
    scale = n
    
def set_translate(n):
    global translate
    translate = n

def set_oximax(n):
    global oximax
    oximax = n


VAR.register(SCALE, lambda: scale, set_scale)
VAR.register(TRANSLATE, lambda: translate, set_translate)
VAR.register(OXIMAX, lambda: oximax, set_oximax)


def ox_signal(GLOBAL_FREQ):

    ind = LinTable([(0,0), (100,1), (1000,.25), (8191,0)])
    # play1 = Trig()
    metroo = Metro(.5).play()
    # tr = TrigEnv(play1, table=ind, dur=120)
    tr = TrigEnv(metroo, table=ind, dur=1)
   
    # signal = CrossFM(carrier=[1000, 1000], ind1=tr, ind2=tr, ratio=0.1, mul=.2)
   
    fadein = 30
    fadeout = GLOBAL_FREQ * 0.4
    dur = GLOBAL_FREQ * 0.6
    fm_fader = Fader(
        fadein=fadein, fadeout=fadeout, dur=dur, mul=0.4
    )
    signal = CrossFM(carrier=[1000, 1000], ind1=tr, ind2=tr, ratio=0.1, mul=fm_fader)
    FN.register("ox/start", lambda: signal.out())
    FN.register("ox/stop", lambda: signal.stop())
    FN.register(FADER, lambda: fm_fader.play())

    VAR.register("ox/vol", lambda: fm_fader.mul, lambda x: fm_fader.setMul(x))
    VAR.register(METRO, lambda: metroo.time, lambda x: metroo.setTime(x))
    return signal


def ox_update(signal, dt, ph, ox, battery, temp):
    VAR.set(OXIMAX, max(ox, VAR.get(OXIMAX)))
    signal.setRatio((ox +0.1 + VAR.get(TRANSLATE)) * VAR.get(SCALE))
    signal.setCarrier([battery +200, battery +201])
    FN.call(FADER)
    VAR.set(METRO, 1.01 - (ox / VAR.get(OXIMAX)) )
