from datetime import datetime
from pyo import SfPlayer, Metro, TrigFunc
from collections import namedtuple

Record = namedtuple("Record", ["key", "start", "len"])

NUMBERS = [
    "zero",
    "un",
    "deux",
    "trois",
    "quatre",
    "cinq",
    "six",
    "sept",
    "huit",
    "neuf",
]

SPECIALS = [
    "onze",
    "douze",
    "treize",
    "quatorze",
    "quinze",
    "seize",
]

TENS = [
    "dix",
    "vingt",
    "trente",
    "quarante",
    "cinquante",
    "soixante",
    "septante",
    ["quatre", "vingt"],
    "nonante",
    "cent",
]


def concat(*args):
    ret = []
    for arg in args:
        for item in arg:
            ret.append(item)
    return ret


class MappingError(Exception):
    pass


def find_mapping(mappings, key):
    for rec in mappings:
        if rec.key == key:
            return rec
    raise MappingError(f'"{key}" not found in mappings')


def parse_u(u, skip_zero=True):
    if skip_zero and u == 0:
        return "silence"
    return NUMBERS[u]


def parse_mil(n):
    if n == 0:
        return []
    elif n == 1:
        return ["mille"]
    return [parse_u(n), "mille"]


def parse_dec(n):
    if n == 0:
        return []
    elif n == 8:
        return TENS[n - 1]
    return [TENS[n - 1]]


def parse_cent(n):
    if n == 0:
        return []
    elif n == 1:
        return ["cent"]
    return [parse_u(n), "cent"]


def parse_special(n):
    return SPECIALS[n - 11]


def parse_int(n):
    if n > 10 and n <= 16:
        return [parse_special(int(n))]
    unit = int(n % 10)
    decims = int((n % 100) // 10)
    cents = int((n % 1000) // 100)
    mils = int((n % 10000) // 1000)
    if n >= 1000:
        return concat(
            parse_mil(mils), parse_cent(cents), parse_dec(decims), [parse_u(unit)]
        )
    elif n >= 100:
        return concat(parse_cent(cents), parse_dec(decims), [parse_u(unit)])
    if n >= 10:
        return concat(parse_dec(decims), [parse_u(unit)])

    return [parse_u(unit, False)]


def parse_marktime(s):
    minutes, seconds = s.split(":")
    return (float(minutes) * 60) + float(seconds)


def parse_mappings(mappings_file):
    import csv

    ret = []
    with open(mappings_file, newline="") as f:
        reader = csv.reader(f)
        for row in reader:
            try:
                key = row[1]
                start = parse_marktime(row[2])
                len = parse_marktime(row[4])
                ret.append(Record(key, start, len))
            except Exception as ex:
                print(f"parse_mappings: {ex}")
                print(f"key: {key}")

    return ret


def parse_date(dt: datetime):
    keys = concat(
        parse_int(dt.hour),
        ["heure"],
        parse_int(dt.minute),
        ["minute"],
    )
    # print(f'{dt.hour}:{dt.minute} -> {keys}')
    return keys


def parse_number(n):
    if type(n) == int:
        return parse_int(n)

    a = int(n)
    b = (n - a) * 100
    return concat(
        parse_int(a),
        ["point"],
        parse_int(b),
    )


class AfterOnce:
    def __init__(self, fn) -> None:
        self.once_flag = False
        self.fn = fn

    def __call__(self):
        # print(f"{datetime.now()}> AfterOnce", self.once_flag)
        if self.once_flag is False:
            self.once_flag = True
        else:
            self.fn()


def after_once(fn):
    return AfterOnce(fn)


class Voice:
    def __init__(self, sound_file, mappings_file) -> None:
        self._records = parse_mappings(mappings_file)
        self._player = SfPlayer(sound_file, speed=1, mul=2, offset=4)
        self._silence_metro = Metro(1).play()
        self._end_metro = Metro(0.01).play()
        self._stop_trig = TrigFunc(self._end_metro, lambda: self._stop())
        self._play_trig = TrigFunc(self._silence_metro, lambda: self._play())
        self._stop_time = None
        self._current = []

    def _stop(self):
        # self._player.setSpeed(0)
        now = datetime.now().timestamp()
        if self._stop_time is not None and self._stop_time <= now:
            # print(
            #     f"{datetime.now()}> voice.stop @ {self._stop_time} >= {now} = {self._stop_time >= now}"
            # )
            self._player.stop()
            self._stop_time = None

    def _play(self):
        # print("play", len(self._current), self._stop_time)
        if len(self._current) > 0 and self._stop_time is None:
            # print("playing", [r.key for r in self._current])
            rec = self._current.pop(0)
            # print(
            #     f"{datetime.now()}> play record: {rec.key} {rec.start} -> {datetime.fromtimestamp(datetime.now().timestamp() + rec.len)}"
            # )
            self._player.setOffset(rec.start)
            self._stop_time = datetime.now().timestamp() + rec.len
            self._player.out()

    def queue_date(self, dt):
        for key in parse_date(dt):
            self._current.append(find_mapping(self._records, key))

    def queue_number(self, n):
        for key in parse_number(n):
            self._current.append(find_mapping(self._records, key))

    def queue_idents(self, *args):
        for key in args:
            try:
                self._current.append(find_mapping(self._records, key))
            except Exception:
                pass

    def play(self):
        self._play()


def robot_number(name):
    n = int(name[5:])
    return n


def update_voice(voice: Voice, names, dt, ph, ox, battery, temp):
    def say_my_name(what):
        name = names[what]
        n = robot_number(name)
        voice.queue_idents("robot")
        voice.queue_number(n)

    try:

        voice.queue_date(dt)

        say_my_name("ox")
        voice.queue_idents("oxygene_dissous")
        voice.queue_number(ox)

        say_my_name("temperature")
        voice.queue_idents("temperature")
        voice.queue_number(temp)
        voice.queue_idents("degre")

        say_my_name("battery")
        voice.queue_idents("batterie")
        voice.queue_number(battery)
        voice.queue_idents("milliampere")

        say_my_name("ph")
        voice.queue_idents("PH")
        voice.queue_number(ph)

        voice.queue_idents("soupir")

        voice.play()
    except Exception as ex:
        print(f"failed voice: {ex}")
