from pyo import *
from vars import VAR, FN

FREQ_SCALE = "bat/freq-scale"

scale = 3


def set_scale(n):
    global scale
    scale = n


VAR.register(FREQ_SCALE, lambda: scale, set_scale)


def bat_signal(GLOBAL_FREQ):
    lfd4 = Sine([0.2, 0.1], mul=0.2, add=0.2)
    signal = SuperSaw(freq=[49, 50], detune=lfd4, bal=0.7, mul=0.25)

    FN.register("bat/start", lambda: signal.out())
    FN.register("bat/stop", lambda: signal.stop())

    VAR.register("bat/vol", lambda: signal.mul, lambda x: signal.setMul(x))
    return signal


def bat_update(signal, dt, ph, ox, battery, temp):
    base = VAR.get(FREQ_SCALE)
    signal.setFreq([(battery + 10 ) * base, (battery + 10) * (base * 0.92)])
