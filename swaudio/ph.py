from pyo import *
from vars import VAR, FN

PH_SCALE = "ph/scale"
PH_SUPER_SCALE = "ph/super-scale"
PH_PLAY = "ph/play"

scale = 7
super_scale = 10


def set_scale(n):
    global scale
    scale = n


def set_super_scale(n):
    global super_scale
    super_scale = n


VAR.register(PH_SCALE, lambda: scale, set_scale)
VAR.register(PH_SUPER_SCALE, lambda: super_scale, set_super_scale)


def ph_signal(GLOBAL_FREQ):
    ind = LinTable([(0, 20), (20, 5), (100, 2), (8191, 1)])
    # play1 = Trig()
    metroo = Metro(1).play()
    # tr = TrigEnv(play1, table=ind, dur=120)
    tr = TrigEnv(metroo, table=ind, dur=1)
    signal = CrossFM(
        carrier=[250.5, 250], ratio=[0.2499, 0.2502], ind1=tr, ind2=tr, mul=0.2
    )

#    FN.register(PH_PLAY, lambda: play1.play())
    FN.register("ph/start", lambda: signal.out())
    FN.register("ph/stop", lambda: signal.stop())

    VAR.register("ph/vol", lambda: signal.mul, lambda x: signal.setMul(x))
    return signal


def ph_update(signal, dt, ph, ox, battery, temp):
    sph = (ph - VAR.get(PH_SCALE)) * 200
    ssph = ph / VAR.get(PH_SUPER_SCALE)
#    FN.call(PH_PLAY)
    signal.setCarrier([sph, sph])
 #   signal.setRatio([temp * 0.1, temp * 0.2])
