from pyo import *
from vars import VAR, FN

SCALE = "temp/scale"

scale = 30


def set_scale(n):
    global scale
    scale = n


VAR.register(SCALE, lambda: scale, set_scale)


def temp_signal(GLOBAL_FREQ):
    ind = LinTable([(0, 20), (20, 5), (100, 2), (8191, 1)])
    # play1 = Trig()
    metroo = Metro(3).play()
    # tr = TrigEnv(play1, table=ind, dur=120)
    tr = TrigEnv(metroo, table=ind, dur=1)
    signal = CrossFM(
        carrier=[250.5, 250], ratio=[0.2499, 0.2502], ind1=tr, ind2=tr, mul=0.4
    )

    FN.register("temp/start", lambda: signal.out())
    FN.register("temp/stop", lambda: signal.stop())

    VAR.register("temp/vol", lambda: signal.mul, lambda x: signal.setMul(x))
    return signal


def temp_update(signal, dt, ph, ox, battery, temp):
    stemp = temp / VAR.get(SCALE)
    signal.setCarrier([stemp *1001, stemp *1000])
    signal.setRatio([1 - stemp, 0.9 - stemp])
    
