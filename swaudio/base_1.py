from typing import Any
from pyo import *
import csv
from datetime import datetime
from json import loads
from voice import Voice
from time import sleep
from vars import VAR,FN



battery_data = []
with open("data/battery.csv", newline="") as f:
    reader = csv.reader(f)
    for row in reader:
        try:
            dt, value = row
            battery_data.append((datetime.fromisoformat(dt), float(value)))
        except Exception:
            print("Failed on:", str(row))
            pass

print(f"Loaded {len(battery_data)} battery_data records")


ph_data = []
with open("data/ph.csv", newline="") as f:
    reader = csv.reader(f)
    for row in reader:
        try:
            dt, value = row
            ph_data.append((datetime.fromisoformat(dt), float(value)))
        except Exception:
            print("Failed on:", str(row))
            pass

print(f"Loaded {len(ph_data)} ph records")

temp_data = []
with open("data/temperature.csv", newline="") as f:
    reader = csv.reader(f)
    for row in reader:
        try:
            dt, value = row
            temp_data.append((datetime.fromisoformat(dt), float(value)))
        except Exception:
            print("Failed on:", str(row))
            pass

print(f"Loaded {len(temp_data)} tempareture records")


oxygen_data = []
with open("data/oxygen.csv", newline="") as f:
    reader = csv.reader(f)
    for row in reader:
        try:
            dt, value = row
            oxygen_data.append((datetime.fromisoformat(dt), float(value)))
        except Exception:
            print("Failed on:", str(row))
            pass

print(f"Loaded {len(oxygen_data)} oxyfen records")


class DataStream:
    def __init__(self, data):
        if len(data) == 0:
            raise Exception("DataStream fed with an empty data list")
        self._index = -1
        self._data = data

    def next(self):
        self._index += 1
        if self._index >= len(self._data):
            self._index = 0
        return self._data[self._index]

    # def __call__(self) -> Any:
    #     datetime, value = self.next()
    #     self._function(datetime, value)

    # def as_pattern(self, time=1):
    #     return Pattern(self, time)


server = Server().boot()
server.amp = 0.5
while server.getIsBooted() is False:
    print("Waiting portaudio server...")
    sleep(1)
server.start()


GLOBAL_FREQ = 2 * 60

# signal 1
# lfo = Sine(freq=4, mul=.02, add=1)
fm_fader3 = Fader(fadein=10, fadeout=GLOBAL_FREQ * 0.5, dur=GLOBAL_FREQ * 0.3, mul=0.1)
fr_base = [100, 99.7]
fr = SigTo(value=fr_base, time=GLOBAL_FREQ * 0.1, init=0)
ind = LinTable([(0,20), (200,5), (1000,2), (8191,1)])
play1 = Trig()
tr = TrigEnv(play1, table=ind, dur=15)
signal1  = CrossFM(carrier=[250.5,250], ratio=[.2499,.2502], ind1=tr, ind2=tr, mul=fm_fader3).out()
signal1.out()

# signal 2
fm_fader = Fader(fadein=15, fadeout=GLOBAL_FREQ * 0.2, dur=GLOBAL_FREQ * 0.3, mul=0.1)
signal2 = FM(carrier=[1000, 1000], ratio=0.1, index=5, mul=fm_fader)


# signal 3
signal3 = Rossler(pitch=[0.1, 0.1], chaos=0.1, mul=0.1)

# signal 4
fm_fader2 = Fader(
    fadein=GLOBAL_FREQ * 0.7, fadeout=GLOBAL_FREQ * 0.1, dur=GLOBAL_FREQ, mul=0.1
)
lfd4 = Sine([0.4, 0.3], mul=0.2, add=0.5)
signal4 = SuperSaw(freq=[49, 50], detune=lfd4, bal=0.7, mul=fm_fader2)


signal1.out()
signal2.out()
signal3.out()
signal4.out()

FN.register('start1', lambda: signal1.out())
FN.register('stop1', lambda: signal1.stop())
FN.register('start2', lambda: signal2.out())
FN.register('stop2', lambda: signal2.stop())
FN.register('start3', lambda: signal3.out())
FN.register('stop3', lambda: signal3.stop())
FN.register('start4', lambda: signal4.out())
FN.register('stop4', lambda: signal4.stop())


VAR.register("amp", lambda: server.amp, lambda x: server.setAmp(x))
FN.register("start", lambda: server.start())
FN.register("stop", lambda: server.stop())


ph_stream = DataStream(ph_data)
ox_stream = DataStream(oxygen_data)
battery_stream = DataStream(battery_data)
temp_stream = DataStream(temp_data)


PH_SCALE = 0.17

voice = Voice("data/voice_yann.wav", "data/regions_markers_voice_yann.csv")



def update():
    dt, ph = ph_stream.next()
    _, ox = ox_stream.next()
    _, battery = battery_stream.next()
    _, temp = temp_stream.next()

    # print("update", ph, ox, battery, temp)
    def say_value(n):
        def inner():
            voice.play_number(n)
        return inner

    def say_ph():
        voice.play_idents("PH", callback=say_value(ph))

    voice.play_date(dt, say_ph)

    fm_fader3.play()
    fr.setValue(fr_base)
    sph = (ph - 7 * 100) 
    ssph = (ph/10)
    play1.play()
    signal1.setCarrier([sph, sph])
    # lfo.setFreq(sph * lf2 * fr)
    # lf2.setFreq( ph / 4)
    # signal1.setFreq([sph, sph] * lf2 * fr)
    play1.play()
    signal1.setCarrier([sph, sph])

    fm_fader.play()
    signal2.setRatio( ox - 12 * 10 / 10)
    signal2.setCarrier([battery, battery])

    stemp = temp / 30
    signal3.setChaos(stemp)
    signal3.setPitch([1 - stemp, stemp / 2])

    fm_fader2.play()
    signal4.setFreq([battery / 6, battery / 5.6])


metro = Metro(GLOBAL_FREQ).play()
tf = TrigFunc(metro, update)
VAR.register("metro", lambda: metro.time, lambda x: metro.setTime(x))

# fm = FM(carrier=1000, ratio=.1, index=5, mul=.2).out()


# def oxygen(datetime, value):
#     scaled = (value - 10) / 10
#     print(f'oxygen {scaled}')
#     # sine_600.setMul(  (value - 10) / 10)
#     fm.ratio += .001

# ox_stream = DataStream(oxygen_data, oxygen)
# p = ox_stream.as_pattern(2)
# p.play()


class ParseError(Exception):
    pass


def parse_input(input_string: str):
    try:
        parts = input_string.strip().split(" ")
        ident = parts[0]
        rest = " ".join(parts[1:]).strip()
        if len(rest) > 0:
            return ident, eval(rest)
        else:
            return ident, None
    except Exception as ex:
        raise ParseError(f"Exception({ex})")


def exec_command(ident, arg):
    if ident == 'save' and type(arg) == str:
        VAR.save(arg)
    elif ident == 'restore' and type(arg) == str:
        VAR.restore(arg)
    elif VAR.has(ident) or ident == "*":
        if arg is None:
            if ident == "*":
                for key in VAR._values.keys():
                    print(f"{key} = {VAR.get(key)}")
            else:
                print(f"{ident} = {VAR.get(ident)}")
        else:
            VAR.set(ident, arg)
        return
    elif FN.has(ident):
        FN.call(ident)


def gui():
    while True:
        try:
            x = input("-> ")
            if len(x.strip()) > 0:
                ident, arg = parse_input(x)
                exec_command(ident, arg)
        except KeyboardInterrupt:
            server.stop()
            print("\nBye!")
            exit(0)
        except Exception as ex:
            print(f"Error: {ex}")


if __name__ == "__main__":
    gui()
