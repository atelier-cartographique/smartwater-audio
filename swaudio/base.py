from pyo import *
from battery import bat_signal, bat_update
from oxygen import ox_signal, ox_update
from ph import ph_signal, ph_update
from temperature import temp_signal, temp_update
from voice import Voice, update_voice
from time import sleep
from vars import VAR, FN
from streams import battery_stream, ox_stream, ph_stream, temp_stream
import asyncio
from datetime import datetime
import os

GLOBAL_FREQ = int(os.environ.get("SW_FREQ", "58"))
OFFLINE = os.environ.get("SW_OFFLINE") is not None


server = Server().boot()
server.amp = 0.5
while server.getIsBooted() is False:
    print("Waiting portaudio server...")
    sleep(1)

VAR.register("amp", lambda: server.amp, lambda x: server.setAmp(x))
FN.register("start", lambda: server.start())
FN.register("stop", lambda: server.stop())

VOICE_FILE = os.environ.get("SW_VOICE", "data/voice.wav")
voice = Voice(VOICE_FILE, "data/regions_markers_voice.csv")

signal_oxygen = ox_signal(GLOBAL_FREQ)
signal_ph = ph_signal(GLOBAL_FREQ)
signal_battery = bat_signal(GLOBAL_FREQ)
signal_temp = temp_signal(GLOBAL_FREQ)

signal_oxygen.out()
signal_ph.out()
signal_battery.out()
signal_temp.out()


class DataHolder:
    def __init__(self):
        self.ph = [datetime.now(), 0, "robot000"]
        self.ox = [datetime.now(), 0, "robot000"]
        self.bat = [datetime.now(), 0, "robot000"]
        self.temp = [datetime.now(), 0, "robot000"]
        self._processed = True

    def is_processed(self):
        return self._processed

    def set_processed(self, s):
        self._processed = s


data_holder = DataHolder()


async def update_data():
    if OFFLINE:
        data_holder.ph = ph_stream.next()
        data_holder.ox = ox_stream.next()
        data_holder.bat = battery_stream.next()
        data_holder.temp = temp_stream.next()
    else:
        ph, ox, bat, temp = await asyncio.gather(
            ph_stream.next(),
            ox_stream.next(),
            battery_stream.next(),
            temp_stream.next(),
        )
        data_holder.ph = ph
        data_holder.ox = ox
        data_holder.bat = bat
        data_holder.temp = temp

    data_holder.set_processed(False)


def update_instruments():
    if data_holder.is_processed():
        return
    data_holder.set_processed(True)
    dt, ph, ph_name = data_holder.ph
    _, ox, ox_name = data_holder.ox
    _, battery, bat_name = data_holder.bat
    _, temp, temp_name = data_holder.temp
    print(f"[{dt}]")
    print(f"ph({ph_name}): {ph}")
    print(f"ox({ox_name}): {ox}")
    print(f"bat({bat_name}): {battery}")
    print(f"temp({temp_name}): {temp}")

    names = {
        "ph": ph_name,
        "ox": ox_name,
        "battery": bat_name,
        "temperature": temp_name,
    }
    ox_update(signal_oxygen, dt, ph, ox, battery, temp)
    ph_update(signal_ph, dt, ph, ox, battery, temp)
    bat_update(signal_battery, dt, ph, ox, battery, temp)
    temp_update(signal_temp, dt, ph, ox, battery, temp)
    update_voice(voice, names, dt, ph, ox, battery, temp)


metro = Metro(GLOBAL_FREQ).play()
# tf = TrigFunc(metro, update)
VAR.register("metro", lambda: metro.time, lambda x: metro.setTime(x))


class ParseError(Exception):
    pass


def parse_input(input_string: str):
    try:
        parts = input_string.strip().split(" ")
        ident = parts[0]
        rest = " ".join(parts[1:]).strip()
        if len(rest) > 0:
            return ident, eval(rest)
        else:
            return ident, None
    except Exception as ex:
        raise ParseError(f"Exception({ex})")


def exec_command(ident, arg):
    if ident == "save" and type(arg) == str:
        VAR.save(arg)
    elif ident == "restore" and type(arg) == str:
        VAR.restore(arg)
    elif ident == "recstart" and type(arg) == str:
        server.recstart(arg)
    elif ident == "recstop":
        server.recstop()
    elif VAR.has(ident) or ident == "*":
        if arg is None:
            if ident == "*":
                for key in VAR._values.keys():
                    print(f"{key} = {VAR.get(key)}")
            else:
                print(f"{ident} = {VAR.get(ident)}")
        else:
            VAR.set(ident, arg)
        return

    elif FN().has(ident) or ident == "?":
        if ident == "?":
            print("save <filename>")
            print("restore <filename>")
            print("recstart <filename>")
            print("recstop")

            for key in FN._fns.keys():
                print(f"{key}")
        else:
            FN.call(ident)


def gui():
    # server.start()
    while True:
        try:
            x = input("-> ")
            if len(x.strip()) > 0:
                ident, arg = parse_input(x)
                exec_command(ident, arg)
        except KeyboardInterrupt:
            server.stop()
            print("\nBye!")
            exit(0)
        except Exception as ex:
            print(f"Error: {ex}")


async def updater():
    print("starting updater")
    while True:
        await update_data()
        # server.stop()
        # update_instruments()
        # server.start()
        await asyncio.sleep(GLOBAL_FREQ)


if __name__ == "__main__":
    # server.setTimeCallable(update_instruments)
    server.setCallback(update_instruments)
    server.start()
    # gui()
    # loop = asyncio.get_event_loop()
    # loop.run_until_complete(updater())
    asyncio.run(updater())
