import csv
from datetime import datetime
import os
import httpx

OFFLINE = os.environ.get("SW_OFFLINE") is not None

battery_data = []
ph_data = []
temp_data = []
oxygen_data = []

if OFFLINE:
    with open("data/battery.csv", newline="") as f:
        reader = csv.reader(f)
        for row in reader:
            try:
                dt, value = row
                battery_data.append((datetime.fromisoformat(dt), float(value)))
            except Exception:
                print("Failed on:", str(row))
                pass

    print(f"Loaded {len(battery_data)} battery_data records")

    with open("data/ph.csv", newline="") as f:
        reader = csv.reader(f)
        for row in reader:
            try:
                dt, value = row
                ph_data.append((datetime.fromisoformat(dt), float(value)))
            except Exception:
                print("Failed on:", str(row))
                pass

    print(f"Loaded {len(ph_data)} ph records")

    with open("data/temperature.csv", newline="") as f:
        reader = csv.reader(f)
        for row in reader:
            try:
                dt, value = row
                temp_data.append((datetime.fromisoformat(dt), float(value)))
            except Exception:
                print("Failed on:", str(row))
                pass

    print(f"Loaded {len(temp_data)} tempareture records")

    with open("data/oxygen.csv", newline="") as f:
        reader = csv.reader(f)
        for row in reader:
            try:
                dt, value = row
                oxygen_data.append((datetime.fromisoformat(dt), float(value)))
            except Exception:
                print("Failed on:", str(row))
                pass

    print(f"Loaded {len(oxygen_data)} oxyfen records")


class DataStream:
    def __init__(self, data):
        if len(data) == 0:
            raise Exception("DataStream fed with an empty data list")
        self._index = -1
        self._data = data

    def next(self):
        self._index += 1
        if self._index >= len(self._data):
            self._index = 0
        dt, value = self._data[self._index]
        return [dt, value, "robot000"]


class RemoteStream:
    def __init__(self, db) -> None:
        self.db = db

    async def next(self):
        try:
            values, name = await get_serie(self.db)
            dt, val = values[-1]
            return dt, val, name
        except Exception as ex:
            print("next error:", ex)
            return [datetime.now(), 0, "robot000"]


if OFFLINE:
    ph_stream = DataStream(ph_data)
    ox_stream = DataStream(oxygen_data)
    battery_stream = DataStream(battery_data)
    temp_stream = DataStream(temp_data)
else:
    ph_stream = RemoteStream("pH")
    ox_stream = RemoteStream("dissolved_oxygen")
    battery_stream = RemoteStream("battery")
    temp_stream = RemoteStream("temperature")


async def get_data(what):
    url = "https://monitoring.smartwater.brussels/grafana/api/datasources/proxy/4/query"
    db = "smartwater_sewer"
    epoch = "ms"
    q = f"""SELECT mean("value") FROM "device_frmpayload_data_{what}" WHERE time >= now() - 30m and time <= now() GROUP BY time(1s), "device_name" fill(none)"""
    async with httpx.AsyncClient() as client:
        try:
            response = await client.get(url, params={"db": db, "epoch": epoch, "q": q})
            # print(f"URL({what}): {response._request.url}")
        except httpx.ConnectTimeout:
            print(f"Timeout error getting {what}")
            return None
        if response.status_code == 200:
            return response.json()
    return None


def deser_row(row):
    return (datetime.fromtimestamp(row[0] / 1000), row[1])


def max_date_serie(serie):
    robot = serie["tags"]["device_name"]
    max_ts = 0
    for ts, _v in serie["values"]:
        max_ts = max(ts, max_ts)
    return max_ts, robot


def get_serie_by_name(name, series):
    for serie in series:
        if serie["tags"]["device_name"] == name:
            return serie["values"]
    return []


class BotPicker:
    def __init__(self) -> None:
        self.bots = []
        self.index = -1

    def add(self, bot):
        if bot not in self.bots:
            self.bots.append(bot)

    def pick(self):
        if len(self.bots) < 1:
            return None
        new_index = self.index + 1
        if new_index >= len(self.bots):
            new_index = 0
        self.index = new_index
        return self.bots[self.index]


bot_picker = BotPicker()


async def get_serie(what):
    data = await get_data(what)
    if data is None:
        print("Data is None")
        return None
    series = data["results"][0]["series"]

    for serie in series:
        bot_picker.add(serie["tags"]["device_name"])
    name = bot_picker.pick()
    serie = get_serie_by_name(name, series)
    # print("winner", name, serie)
    values = [deser_row(row) for row in serie]
    return values, name
